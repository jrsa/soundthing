from operator import add
from functools import reduce

import numpy as np

from .ugen import Ugen


class Sum(Ugen):
    def tick(self, n):
        if self.inports:
            return sum([port.tick(n) for port in self.inports])
        else:
            # XXX hardcoded stereo
            return np.zeros((n, 2))
