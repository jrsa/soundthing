from numpy import ndarray, zeros, uint16

from .ugen import Ugen


class Dumb(Ugen):
    def __init__(self):
        self.buffer = None

    def set(self, buffer: ndarray):
        self.buffer = buffer
        # print(f'{type(buffer)} {buffer}')

    def tick(self, n):
        # XXX harcoded for stereo
        # also, i really gotta do this? for python lists, emptiness is truthiness. 
        # i guess numpy said no to that...
        if isinstance(self.buffer, ndarray):
            # assert self.buffer.shape == (n, 2)
            assert self.buffer.shape[0] == n
            return self.buffer
        else:
            # return zeros((n, 2)).astype(uint16)
            raise
            return zeros((n, 2))
