class Ugen:
    def __init__(self):
        self._evts = []
        self.inports = []

    def __rshift__(self, rhs):
        print(f'connecting {self} to {rhs}')
        rhs.inports.append(self)

    def tick(self):
        raise RuntimeError('ugen doesnt have tick() defined')

