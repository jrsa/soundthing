from functools import reduce
from operator import add

import numpy as np
import scipy.io.wavfile as spwf

from .ugen import Ugen

class Buffer(Ugen):
    def __init__(self):
        super().__init__()
        # any buffer should have these
        # self.data = np.zeros(4096)
        self.data = np.random.random(44100) * 10.
        self.pos = 0
        self.fs = 44100

        # self.incr = 1.0

        # only for looping
        self.slide = 1000
        self.looplength = 10000
        self._init_slide()
        self.loop = True

    def _init_slide(self):
        self.loopstart = 0
        self.pos = 0

    # TODO: opposite, write()
    # also doing interpolation would complicate this quite a bit
    def tick(self, nframes): # , ctl):

        # this is very goofy right now,
        slide_ctl = self.inports[0].tick(nframes)[0][2]
        looplength_ctl = self.inports[0].tick(nframes)[0][3]
        result =  self.data[self.pos:self.pos + nframes]

        if len(result) != nframes:
            self._evts.append(('underrun', len(result)))

        self.pos += result.shape[0]

        # update parameters, pretty jank right now, we basically have an array of floats...
        self.slide = int(slide_ctl * self.looplength * 2)
        self.looplength = int(looplength_ctl * self.fs * 2)

        # check if current loop is done
        if self.loop and self.pos > (self.loopstart + self.looplength):
            self._evts.append(('loop',))
            self.loopstart += self.slide
            self.pos = self.loopstart

        # handle wrap (this entails zeroing the index and incrementing again by the wrap amount):
        wrap = nframes - result.shape[0]
        if wrap:
            self._init_slide()
            self._evts.append(('wrap',))
            wrapped_read = self.data[self.pos:self.pos + wrap]
            result = np.hstack((result, wrapped_read))
            self.pos += wrapped_read.shape[0]

        # result *= amp
        # result = result.astype(np.int16)
        # XXX duplicating single channel, dont have to do this if the rest of the class is fixed 
        return np.column_stack([result] * 2)

    def tell(self):
        return self.pos

    def setpos(self, pos):
        self.pos = pos

    def load(self, fn):
        self.fs, data = spwf.read(fn)

        # NOTE: for now, force data to be 16 bit, since the top level is hard coded to use this for realtime i/o.
        # we could actually use int32 internally regardless (or even floats) and then still convert to/from 16
        # (or whatever the card can do) in the callback. need to also make sure to scale the samples and not just
        # change the numeric type.

        # NOTE2: regardless of where the conversion happens, the scipy function just gives an array of int32s without telling
        # us whether it started as 24 bits or 32. shift by 8 and see if it's still clipping ballz?? lol
        if data.dtype == np.int32:
            print(f'warning: trying to scale {fn} to lower bit depth')
            data = data / 100000
            data = data.astype(np.int16)

        if True:
            # data is a 1-D array. mono file is used as-is, only left channelf from stereo file is used
            if len(data.shape) > 1:
                x = data.T[0]
            else:
                x = data.T
        else:
            # XXX this is the right way to do it, rest of class has to be fixed up though
            # data is a 2-D array, samples x channels
            if len(data.shape) == 1:
                x = data.reshape(data.shape[0], 1).T
            else:
                x = data.T

        self.data = x
        print(f'loaded {fn}: {self.data.shape} fs: {self.fs}')
