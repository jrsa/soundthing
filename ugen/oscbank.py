from functools import reduce
from operator import add

import numpy as np

from .ugen import Ugen


class OscBank(Ugen):
    def __init__(self):
        super().__init__()
        self.i = 0

    # def tick(self, in_data, ctl):
    # im gonna use the input ports for ctl data
    def tick(self, n):

        # n = in_data.shape[0]
        phase = np.linspace(self.i, self.i + n - 1, n)

        amp = 2 ** 7

        # phs_spread = ctl[3] * 300
        phs_spread = 150

        # partials = np.linspace(ctl[2], ctl[1] * 100, int(ctl[0] * 800) + 1)
        partials = np.linspace(1, 100, 400)

        dataL = reduce(add, (np.sin((phase + phs_spread) / i) for i in partials)) * amp
        dataR = reduce(add, (np.sin((phase - phs_spread) / i) for i in partials)) * amp

        data = np.column_stack((dataL, dataR))

        # data = data.astype(np.int16)
        self.i += n
        return data
