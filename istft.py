from scipy.fftpack import fft, ifft
import numpy as np

'''
turn X back into x, we need to know the hop size that was used for the forward 
transform, and we need to end up with (X.shape[0] / hop_size) * X.shape[1]) output samples
'''
def istft(X, hop_size=2):
    frame_length = X.shape[1]
    n_frames = X.shape[0]

    # make a buffer for the output to be OLAdded into
    out = np.zeros(frame_length * ((n_frames // hop_size) + 1))
    hop_len = frame_length // hop_size

    # compute inverse fft and overlap-add the results
    for i in range(n_frames):
        i_frame = i * hop_len

        # the division by the frame length is for normalization. i remember
        # hearing that you need to do that after you compute an inverse fft
        out[i_frame:i_frame + frame_length] += np.real(ifft(X[i]) / frame_length)
    return out


def istft_single(X_n):
    frame_length = X.shape[0]

    out = np.zeros(frame_length * ((n_frames // hop_size) + 1))
    hop_len = frame_length // hop_size

    for i in range(n_frames):
        i_frame = i * hop_len
        out[i_frame:i_frame + frame_length] += np.real(ifft(X[i]) / frame_length)

    return out


