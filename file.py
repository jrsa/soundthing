import sys
import scipy.io.wavfile as spwf


if __name__ == '__main__':
    for filename in sys.argv[1:]:
        try:
            fs, data = spwf.read(filename)
            print(f'{filename} {fs} {data.dtype}')
        except Exception as e:
            print(f'{filename} ERROR: {e}')
