import os.path
import sys
import time

import pyaudio

import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as spwf

import jmir

from istft import istft

from scipy.fftpack import  ifft

from operator import add
from functools import reduce


class Out:
    def __init__(self, buf):
        # index into output buffer
        self.i = 0
        self._buf = buf

    '''
    return one buffer's worth (sized n) of `np.sin()`
    this is also an example of scaling and converting samples before they go to the sound card
    '''
    def tick(self, n):
        amp = 2 ** 9

        # no resampling or interpolation whatsoever
        data = self._buf[self.i:self.i + n] * amp

        data = data.astype(np.int16)
        self.i += n
        return data

'''
phase vocoder, using my stft code from MIR class. unfortunately MIR work doesn't
usually call for modifying and resynthesizing the spectral data...
'''
class Pvoc:
    def __init__(self, N=1024):
        self._N = N
        self._i = 0

    def load(self, fn):
        self.fs, data = spwf.read(fn)

        # need to branch here for mono vs stereo files
        if len(data.shape) > 1:
            x = data.T[0]
        else:
            x = data.T
        self.spectrum_data = jmir.stft.stft(x, N=self._N, truncate_imag=False)

    def plot(self):
        jmir.stft.plot(self.spectrum_data)

    def out(self):
        return istft(self.spectrum_data)

    def save(self, fn):
        spwf.write(fn, self.fs, self.out())


class PvocTap:
    def _create_buffers(self):
        # i originally had the size of these multiplied by 4. i dont actually have a good reason for
        # that, and there were weird artifacts that i had trouble resolving, so im leaving them like
        # this for now. there might be reasons to have longer buffers in the future, and it could
        # probably be fixed up to work.
        self._ola_write_buffer = np.zeros(self._src_pvoc.spectrum_data.shape[1])
        self._ola_read_buffer = np.zeros(self._src_pvoc.spectrum_data.shape[1])

    def __init__(self, src, incr=1):
        # pvoc object (will read from its `spectrum_data`)
        self._src_pvoc = src

        # time index at which to sample from the spectrum data
        # right now, the input data is being loaded in once, from a file, so we
        # don't need "read"/"write" heads
        self._frame_index = 0
        self._frame_increment = incr

        # we will try to keep this full of resynthesized time-domain data
        self._create_buffers()
        self._ola_read_index = 0
        self._ola_write_index = 0
        self._ola_factor = 2

    def _swap(self):
        x = self._ola_write_buffer
        self._ola_write_buffer = self._ola_read_buffer
        self._ola_read_buffer = x

    def __repr__(self):
        return (
            f'<PvocTap src={self._src_pvoc.spectrum_data.shape}' +
            f' _frame_index={self._frame_index}' +
            f' _ola_read_index={self._ola_read_index}' +
            f' _ola_write_index={self._ola_write_index}>'
        )

    def read(self, n):
        amp = 2 ** 9

        self._internal_fill()
        self._internal_fill()
        # self._internal_fill()

        # overlap-add buffer read ####################################################### 
        data = self._ola_read_buffer[self._ola_read_index:self._ola_read_index + n]

        # check how much was actually read, and increment the read head accordingly
        nread_from_ola = data.shape[0]
        self._ola_read_index += nread_from_ola

        # handle wrap (this entails zeroing the index and incrementing again by the wrap amount):
        wrap = n - nread_from_ola
        if wrap:
            self._ola_read_index = 0
            wrapped_read = self._ola_read_buffer[self._ola_read_index:self._ola_read_index + wrap]
            data = np.hstack((data, wrapped_read))
            self._ola_read_index += wrapped_read.shape[0]

        data *= amp
        data = data.astype(np.int16)
        return data


    def _get_next_frame(self):
        # wrap frame index
        if self._frame_index >= self._src_pvoc.spectrum_data.shape[0]:
            # print('!spectral frame wrap!')
            self._frame_index = 0

        # linearly interpolate adjacent frames
        fract, floor = np.modf(self._frame_index)

        frame0_in = self._src_pvoc.spectrum_data[int(floor) - 1]
        # frame1_in = self._src_pvoc.spectrum_data[int(floor)]

        # right now fract is a scalar, and is being broadcast across the frames for a constant blending
        # factor. i think i can make fract vary across the bins:
        fract = (np.random.random(fract.shape))
        frame_in = (frame1_in * fract) + (frame0_in * (1.0 - fract))

        # doing rect -> pol -> rect entails a lot of cpu intensive trig calcs, approximately DOUBLING
        # the cpu usage of the tap

        # rect -> pol
        # mag = np.absolute(frame_in)
        # phs = np.angle(frame_in)

        # pol -> rect
        # frame_out = mag * ( np.cos(phs) + np.sin(phs)*1j )

        frame_out = frame_in

        transformed = np.real(ifft(frame_out) / self._src_pvoc.spectrum_data.shape[1])
        self._frame_index += self._frame_increment
        return transformed / 4

    
    '''
    ifft and overlap-add the stft data
    '''
    def _internal_fill(self):
        frame_out = self._get_next_frame()

        end_index = self._ola_write_index + frame_out.shape[0]

        # so after looking at pretty plots, i think i understand what needs to be done:
        # 1) keep two buffers (widely known as double buffering... :p)
        # 2) after one has been filled, it will be marked as ready for the sound card, and the rest of the current frame
        #    will need to be written into the other buffer after the swap

        if end_index > self._ola_write_buffer.shape[0]: # need to do two separate writes, one before wrap and one after
            # write into end of ola buffer
            wrap_amount = self._ola_write_buffer.shape[0] - self._ola_write_index
            wrapped_end_index = end_index - self._ola_write_buffer.shape[0]

            pre_wrap_in = frame_out[:wrap_amount]
            post_wrap_in = frame_out[self._ola_write_buffer.shape[0] - self._ola_write_index:]

            self._ola_write_buffer[self._ola_write_index:] += pre_wrap_in
            #
            # (!) swap double buffer here, write post-wrapped portion into new, clean buffer, and 
            # make the old, full buffer available to the sound card
            #
            self._swap()

            # we're now writing into the other buffer
            self._ola_write_buffer[:] = 0.
            self._ola_write_buffer[:wrapped_end_index] += post_wrap_in

            # warning this will only work if the buffer sizes are nice multiples of each other... or something
            self._ola_write_index = (frame_out.shape[0] // self._ola_factor) - wrap_amount


        else: # non-wrapped case
            self._ola_write_buffer[self._ola_write_index:end_index] += frame_out

            # we dont start the next write at the same index that we ended this 
            # one at, hence the "overlap" part...
            self._ola_write_index += (frame_out.shape[0] // self._ola_factor) 
            # 2 is our "overlap factor", surely that should be a field on the class or something

def stream(callback, fs):
    p = pyaudio.PyAudio()

    stream = p.open(format=p.get_format_from_width(2),
                    channels=1,
                    rate=fs,
                    output=True,
                    stream_callback=callback)

    stream.start_stream()

    print('i/o buffer size:', stream._frames_per_buffer)

    try:
        while stream.is_active():
            print(f'cpu load: {round(stream.get_cpu_load(), 2) * 100}%')
            time.sleep(0.5)
    except KeyboardInterrupt:
        print(f'bye')

    finally:
        stream.stop_stream()
        stream.close()
        p.terminate()


def main(input_fn):
    manipulate_spectrum = False
    show_plot = False
    play = True
    save = False

    ntaps = 10

    pv = Pvoc()
    pv.load(input_fn)

    # taps = [PvocTap(src=pv, incr=i) for i in np.linspace(0.9, 1.1, ntaps)]
    taps = [PvocTap(src=pv, incr=1.0)]

    # zero first k / 10 bins
    if manipulate_spectrum:
        leng = pv.spectrum_data.shape[1]

        # this is how you zero bins when the spectral data is mirrored.
        bin_ = 1
        pv.spectrum_data[:, 0:bin_] = 0
        pv.spectrum_data[:, -bin_:] = 0

    # plot spectrum data
    if show_plot:
        pv.plot()
        plt.show()

    # write out wave file of pvoc'd buffer
    if save:
        name, ext = os.path.splitext(os.path.basename(input_fn))
        output_fn = os.path.join('.', f'{name}_out.wav')
        print(f'writing test result to {output_fn}')
        pv.save(output_fn)

    if play:
        def audio_callback(in_data, frame_count, time_info, status):
            data = reduce(add, [t.read(frame_count)  for t in taps], 0)
            return (data, pyaudio.paContinue)

        stream(callback=audio_callback, fs=pv.fs)
   

if __name__ == "__main__":
    t = time.time()
    try:
        fn = sys.argv[1]
    except IndexError:
        print('specify input wav filename as argument')
    else:
        main(fn)
    print(time.time() - t)
