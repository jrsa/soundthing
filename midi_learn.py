#! /usr/bin/python

from glob import glob
from os import open, O_RDONLY, read
from pprint import pprint
from select import select
from sys import exit

def main():
    
    # find first midi controller
    midis = glob('/dev/midi*')
    if midis:
        midi = midis[0]
        print(f'opening {midi}...')
        midi_ctrl = open(midi, O_RDONLY)
    else:
        print('no midi devices found')
        exit(1)

    msgs = []
    cc = {}
    other = {}

    while True:
        readfds, _, _ = select([midi_ctrl], [], [], 0.1)
        if midi_ctrl in readfds:
            msg = list(read(midi_ctrl, 3))
            msgs.append(msg)

            # if msg.type == CC:
            if msg[0] == 176:
                cc[msg[1]] = float(msg[2]) / 127

            # else:
            #     other[msg[0]] = other.get(msg[0], []) + [{msg[1] : msg[2]}]


        # this clears the whole screen and resets the cursor position but is very flickery.
        # might be better to just reset the cursor position and write over what's there.
        print(chr(0o33))
        print(chr(0o143))

        print('cc:')
        pprint(cc)
        print('other:')
        pprint(other)

if __name__ == '__main__':
    main()
