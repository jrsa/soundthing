from argparse import ArgumentParser
from time import sleep
from os import open, read, O_RDONLY
from pprint import pprint
from select import select
from signal import signal, SIGINT
from sys import argv, exit, platform
from threading import Lock

from click import getchar
import pyaudio
import numpy as np

from ugen import Buffer, OscBank, Sum, Dumb


def main():
    ap = ArgumentParser()
    ap.add_argument('filename', nargs='?')
    ap.add_argument('-d', '--device', type=int, help='index of audio device to use, run with -l option to list them')
    ap.add_argument('-m', '--midi-device', type=str, help='path to midi char device ON LINUX ONLY')
    ap.add_argument('-l', '--list', help='list i/o idxes and exit', action='store_true')
    args = ap.parse_args()

    p = pyaudio.PyAudio()

    if args.list:
        pprint([(n ,p.get_device_info_by_index(n)['name']) for n in range(p.get_device_count())])
        default_output_index = p.get_default_output_device_info()['index']
        default_input_index = p.get_default_input_device_info()['index']
        p.terminate()
        exit(0)

    # TODO: unified "toplevel" with pyaudio streaming wrapped in some kind of `Out` class
    out = Sum()
    # adc = Dumb()
    # (oscs := OscBank()) >> (out := Sum())
    (adc := Dumb()) >> out
    (u := Buffer()) >> out
    (midi_ := Dumb()) >> u

    # TODO: Buffer class crashes if you dont load a file into it. i think it makes sense to have it be functional
    # when empty, in case you just want to record into it. add some sane initial values for size and sample rate
    # in there (sample rate should somehow be set globally for the entire process)
    if args.filename:
        u.load(args.filename)

    # midi only on linux
    is_linux = platform == 'linux'
    midi_device_name = (is_linux and args.midi_device) or '/dev/null'
    midi_ctrl = open(midi_device_name, O_RDONLY)
    ctl = [0.] * 128
    ctl_lock = Lock()

    fs = 44100
    num_channels = 2

    running = True

    # ctrl-c handler
    # TODO: sometimes this doesn't exit cleanly (it crashes the audio thread)
    # this should set a flag that is read in the audio thread so that it can exit cleanly
    def handler_(sig, frame):
        nonlocal running
        running = False
    signal(SIGINT, handler_)


    def callback(in_data, frame_count, time_info, status):
        # for stereo, data needs to be interleaved like [L, R, L, R, L, R]
        # i prefer that the conversion to and from numpy arrays such as [[L, R], [L, R]]
        # happen only in this function (the audio callback) and not in the ugen graph
        data = np.frombuffer(in_data, dtype=np.int16).reshape(frame_count, num_channels)
        adc.set(data)

        with ctl_lock:
            ctl_ = ctl.copy()

        # XXX ah yes, i remember now, i need to broadcast the array of cc values "along" the sample axis
        # because there will be one set of midi values per block. they should probably also be smoothed
        # ctl_ is the raw array of values, dont pass that in directly (i added a type hint to the set method
        # to prevent that, but apparently that doesnt work...)

        # yeet
        midi_array = np.array(ctl_).reshape((len(ctl_), 1)).repeat(frame_count, axis=1).T
        #print(midi_array)
        midi_.set(midi_array)

        out_data = out.tick(frame_count).astype(np.int16)

        # the flatten call assumes that we have [[L, R], [L, R]] data, rather than [[L, L], [R, R]]
        return (out_data.flatten(), pyaudio.paContinue if running else pyaudio.paComplete)

    if False: # run audio callback once in main thread to check for gross errors
        N = 512
        ret = callback(np.zeros(N * 2).astype(np.uint16).tobytes('C'), N, None, None)

    stream = p.open(format=p.get_format_from_width(2),
                    channels=num_channels,
                    rate=fs,
                    output=True,
                    input=True,
                    # XXX: add another argument for input? i think the following is correct if you want to use the same device for input and output
                    input_device_index=args.device,
                    output_device_index=args.device,
                    stream_callback=callback)

    stream.start_stream()

    while stream.is_active():
        # cmd = getchar()
        # if cmd == 'q':
        #     running = False

        ################################################################################ 
        # handle midi i/o
        if is_linux:
            readfds, _, _ = select([midi_ctrl], [], [], 0.1)
            if midi_ctrl in readfds:
                msg = list(read(midi_ctrl, 3))

                # NOTE: hardcoded to get CC data from a specific controller
                if msg and False: # and msg[0] == 176:
                    print(msg)
                if msg and msg[0] == 176: # and msg[1] in range(16, 32):
                    with ctl_lock:
                        ctl[msg[1]] = float(msg[2]) / 127
        else:
            sleep(0.1)
        ################################################################################ 

        # show user interface
        # clear screen
        # print(chr(0o33))
        # print(chr(0o143))

        # move cursor to 0, 0
        print(chr(0o33))
        print('[2J')

        print(ctl)

        print(f'cpu load: {round(stream.get_cpu_load() * 100, 2)}%')
        # while u._evts and (e := u._evts.pop()):
        #     print('event: ', e)

    stream.stop_stream()
    stream.close()

    p.terminate()

if __name__ == '__main__':
    main()
