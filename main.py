"""PyAudio Example: Play a wave file (callback version)."""

import pyaudio
import wave
import time
import sys

from math import modf

def lerp(v0, v1, t):
    return ((1.0 - t) * v0) + (t * v1)

class Buffer:
    def __init__(self, databytes, bitdepth, chan):
        self.databytes = databytes
        self.bitdepth = bitdepth
        self.chan = chan

        self.pos = 0
        self.incr = 1.0

        self._slide = 1000
        self._loopstart = 0
        self._loopend = 10000
        self._init_slide()

    def _init_slide(self):
        self.slide = self._slide
        self.loopstart = self._loopstart
        self.loopend = self._loopend
        self.pos = 0

    def read(self, nframes):

        # how big the result should be (in bytes, not samples)
        bytecount = nframes * self.bitdepth * self.chan

        result = [0] * bytecount

        result_byte_index = 0
        source_byte_index = self.pos
        for n in range(nframes):

            #              2 (16bit)       1 (or 2 for stereo)
            for b in range(self.bitdepth * self.chan):
                try:
                    result[result_byte_index] = self.databytes[source_byte_index]
                except IndexError:
                    result[result_byte_index] = 0

                source_byte_index += 1
                result_byte_index += 1

        # wave pos is confusingly also in bytes
        self.pos += bytecount

        # ...meaning that loop parameters are also in bytes
        if self.pos > self.loopend:
            self.loopend = self.loopend + self.slide
            self.loopstart += self.slide
            self.pos = self.loopstart

        if self.pos >= len(self.databytes):
            self._init_slide()

        return bytes(result)

    def tell(self):
        return self.pos

    def setpos(self, pos):
        self.pos = pos

if len(sys.argv) < 2:
    print("Plays a wave file.\n\nUsage: %s filename.wav" % sys.argv[0])
    sys.exit(-1)

wf = wave.open(sys.argv[1], 'rb')

# swallow the whole wav, not a good long term solution (or maybe it is, idk)
b = Buffer(wf.readframes(wf.getnframes()), wf.getsampwidth(), wf.getnchannels())
wf.setpos(0)

p = pyaudio.PyAudio()

def callback(in_data, frame_count, time_info, status):
    data = b.read(frame_count)
    return (data, pyaudio.paContinue)

stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                channels=wf.getnchannels(),
                rate=wf.getframerate(),
                output=True,
                stream_callback=callback)

stream.start_stream()

while stream.is_active():
    time.sleep(0.1)

stream.stop_stream()
stream.close()
wf.close()

p.terminate()
