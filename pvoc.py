import numpy as np
import matplotlib.pyplot as plt

import scipy.io.wavfile as spwf

import os.path
import sys

import time


'''
FYI: this class does not actually implement a "pvoc", which would require an
STFT (short-time frequency transform, successive DFTs "sliding" over the input),
rather than a single DFT of the entire input.
'''
class Pvoc:
    def load(self, fn):
        self.fs, data = spwf.read(fn)

        # take first channel of input data
        # (mono files are NOT handled by this code)
        x = data.T[0]

        self.spectrum_data = np.fft.rfft(x, norm='ortho')

    def plot(self):
        plt.plot(self.spectrum_data)

    def save(self, fn):
        x2 = np.fft.irfft(self.spectrum_data, norm='ortho')
        output_data = np.real(x2).astype(np.int16)
        spwf.write(fn, self.fs, output_data)


def main(input_fn):
    pv = Pvoc()
    pv.load(input_fn)

    # zero first k / 10 bins
    pv.spectrum_data[:pv.spectrum_data.shape[0] // 10] = 0

    name, ext = os.path.splitext(os.path.basename(input_fn))
    output_fn = os.path.join('.', f'{name}_out.wav')

    print(f'writing test result to {output_fn}')
    pv.save(output_fn)


if __name__ == "__main__":
    t = time.time()
    try:
        fn = sys.argv[1]
    except IndexError:
        print('specify input wav filename as argument')
    else:
        main(fn)
    print(time.time() - t)
