import glob
import os.path

SAMP_DIR = '/Users/jrsa/Desktop/soundz/JAMES\'s samp'


def list():
    return glob.glob(join(SAMP_DIR, '*.wav'))

def grab(pattern):
    matches = [f for f in list() if pattern in os.path.basename(f)]
